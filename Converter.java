
public class Converter {
	/* Artin Rezaee
	* Amine Benaceur
	 * Meryem Amouzai
	 */
	
	private double celsiusToFahrenheit(double C){
        
        double Far= ((C*9) / 5) +32;
		
		return Far;
		}
		private double fahrenheitToCelsius(double F){
		 // TODO: The second student will implement this method
		
			
			double Cel = (F-32)*5/9;
			return 	Cel;
		}

		public static void main(String[] args) {
			
			Converter convert = new Converter();
			
		// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
			convert.celsiusToFahrenheit(180.0);
			
		// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
			convert.fahrenheitToCelsius(250.0);
	}
}
